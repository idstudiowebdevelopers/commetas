<?php

namespace ComMetas\Validators;

use Prettus\Validator\LaravelValidator;

class ClienteValidator extends LaravelValidator
{

    protected $rules = [
        'nome' => 'required|max:255',
        'email' => 'required|email'
    ];

}