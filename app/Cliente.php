<?php

namespace ComMetas;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
	// Foi preciso para liberar o tinker preencher os dados de uma forma mais rápida
    protected $fillable = [

	   	'nome',
	   	'email',
	   	'senha'
    ];
}
