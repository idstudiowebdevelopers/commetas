<?php

namespace ComMetas\Http\Controllers;

// interface
use ComMetas\Repositories\ClienteRepository;
use ComMetas\Services\ClienteService;
use Illuminate\Http\Request;

class ClienteController extends Controller
{
    /**
     * @var ClienteRepository
     */
    private $repository;
    /**
     * @var ClienteService
     */
    private $service;

    /**
     * @param ClienteRepository $repository
     * @param ClienteService $service
     */

    public function __construct(ClienteRepository $repository, ClienteService $service)
    {
        $this->repository = $repository;
        $this->service = $service;
    }

   public function index()
   {
   		return $this->repository->all();
   }

    /**
     * @param Request $request
     * @return mixed
     */

   public function store(Request $request)
   {
   		return $this->service->create($request->all());
   }

    /**
     * @param $id
     * @return mixed
     */

   public function show($id)
   {
   		return $this->repository->find($id);
   }

    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */

   public function update(Request $request, $id)
   {
       return $this->service->update($request->all(),$id);
   }

    /**
     * @param $id
     * @return mixed
     */

   public function destroy($id)
   {
   		return $this->repository->delete($id);
   }
    
}
