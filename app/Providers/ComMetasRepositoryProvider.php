<?php

namespace ComMetas\Providers;

use Illuminate\Support\ServiceProvider;

class ComMetasRepositoryProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        // toda vez que chamar o ClienteReposotory ele vai instanciar o clienteRepositoryEloquent
        $this->app->bind(\ComMetas\Repositories\ClienteRepository::class,
                         \ComMetas\Repositories\ClienteRepositoryEloquent::class);
    }
}
