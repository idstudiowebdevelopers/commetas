<?php

use Illuminate\Database\Seeder;

class ClienteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \ComMetas\Entities\Cliente::truncate();
    	factory(\ComMetas\Entities\Cliente::class, 10)->create();
    }
}
